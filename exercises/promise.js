let myPromise = new Promise(function(resolve, reject) {
    setTimeout(function() {
        if (Math.random() > 0.5) {
        resolve();
        } else {
        reject ();
        }
    }, 1000);
    });

    myPromise
    .then(function() {
    console.log('success');
    })

    .catch(function() {
    console.log('fail');
    })

    .then(function() {
    console.log('complete');
    });

    // let secondPromise = myPromise.then(function() {
    //     console.log('success');
    //     });

    //     let thirdPromise = secondPromise.catch(function() {
    //     console.log('fail');
    //     });

    //     thirdPromise.then(function() {
    //     console.log('complete');
    //     });