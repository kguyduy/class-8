// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
// create api-key.js file with const API_KEY="your_api_key" in this same directory to use

// // const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';
// let startURL = 'https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json';
let startURL = 'https://api.nytimes.com/svc/books/v3/lists';
const endURL = `hardcover-fiction.json?api-key=${API_KEY}`;

const displayBooks = function () {
    let year = document.getElementsByName('year')[0].value;
    let month = document.getElementsByName('month')[0].value;
    let date = document.getElementsByName('date')[0].value;

    let newURL = `${startURL}/${year}-${month}-${date}/${endURL}`;

fetch(newURL)
.then(function(response) {
    return response.json();
})
.then(function(responseJson) {
    console.log(responseJson);
    // let book = responseJson.results.books[0];
    const numberOfBooksToShow = Math.min(responseJson.results.books.length, 5);
    for (i = 0; i < numberOfBooksToShow; i++) {
        const book = responseJson.results.books[i];
        const li = document.createElement('li');
        li.innerHTML +=`<img src="${book.book_image}" alt="${book.title}"><h1>${book.title}</h1><p>${book.author}</p><p class="my-3">${book.description}</p><p><a href="${book.amazon_product_url}">Buy now</a></p>`;
        document.getElementById("book-list").appendChild(li);
    }
});
};

const formSearch = document.getElementById('search-form')
.addEventListener('submit', function (e) {
    displayBooks();
    e.preventDefault();
});